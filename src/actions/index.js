import _ from 'lodash';
import jsonPlaceholder from '../apis/jsonPlaceholder';

export const fetchPostsAndUsers = () => async (dispatch, getState) => {
    await dispatch(fetchPosts());
 
//  mesma coisa que o chain    
//    const userIds = _.uniq(_.map(getState().posts,'userId'));
//    userIds.forEach(id => dispatch(fetchUser(id)));

    _.chain(getState().posts)
        .map('userId')
        .uniq()
        .forEach(id => dispatch(fetchUser(id)))
        .value();
};

export const fetchPosts = () => async dispatch => {
    const response = await jsonPlaceholder.get('/posts');
 
    dispatch({type: 'FETCH_POSTS', payload : response.data})
};

export const fetchUser = (id) => async dispatch => {
    const response = await jsonPlaceholder.get(`/users/${id}`);
 
    dispatch({type: 'FETCH_USER', payload : response.data})
};

//memoize
/* 
export const fetchUser = (id) => dispatch => _fetchUser(id, dispatch);
const _fetchUser = _.memoize(async (id, dispatch) => {
    const response = await jsonPlaceholder.get(`/users/${id}`);
 
    dispatch({type: 'FETCH_USER', payload : response.data})
});*/

/*
export const fetchPosts = () => {
    return function(dispatch, getState){
        const promise = jsonPlaceholder.get('/post');

        return {
            type: 'FETCH_POSTS',
            payload: promise
        };
    };
}; */

//Podemos continuar a fazer Actions sem o thunk que retornam objectos
//Esta nao é necessaria
export const selectPost = () => {
    return {
        type: 'SELECT_POST'
    }
};

